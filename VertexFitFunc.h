#ifndef __VERTEXFITFUNC__
#define __VERTEXFITFUNC__

#include "FunctionGradHessian.h"
#include "SimpleTrack3D.h"


class HelixDCAFunc : public FitNewton::FunctionGradHessian
{
  public:
    HelixDCAFunc();
    ~HelixDCAFunc();
    
    bool calcValGradHessian(const Eigen::VectorXd& x, double& val, Eigen::VectorXd& grad, Eigen::MatrixXd& hessian);
    
    FitNewton::FunctionGradHessian* Clone() const {return new HelixDCAFunc();}
    
    double getTangent(unsigned int coor){return tangent[coor];}
    double getPoint(unsigned int coor){return point[coor];}
    
  private:
    std::vector<double> tangent;
    std::vector<double> point;
};

class VertexFitFunc : public FitNewton::FunctionGradHessian
{
  public:
    VertexFitFunc();
    ~VertexFitFunc();
    
    bool calcValGradHessian(const Eigen::VectorXd& x, double& val, Eigen::VectorXd& grad, Eigen::MatrixXd& hessian);
    
    FitNewton::FunctionGradHessian* Clone() const {return new VertexFitFunc();}
    
    void setTracks(std::vector<SimpleTrack3D>* trks){tracks = trks;}
    
  private:
    std::vector<SimpleTrack3D> *tracks;
};


#endif
