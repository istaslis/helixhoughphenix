#!/bin/bash

g++ -O3 -pipe  `root-config --cflags` `pkg-config --cflags eigen3` `pkg-config --cflags SimpleTrack` -c fastTrackMC.cpp

g++ -o fastTrackMC fastTrackMC.o `root-config --libs` `pkg-config --libs SimpleTrack`

