#!/bin/bash

g++ -O3 -pipe  `root-config --cflags` `pkg-config --cflags eigen3` -c particle_gun.cpp

g++ -o particle_gun particle_gun.o `root-config --libs`

