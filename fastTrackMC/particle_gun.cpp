#include "TF1.h"
#include "TRandom3.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include "TMath.h"
#include <cmath>


using namespace std;


int main(int argc, char** argv)
{
  stringstream ss;
  unsigned int nevents = 0;
  ss.clear();ss.str("");ss<<argv[1];ss>>nevents;
  unsigned int tracks = 0;
  ss.clear();ss.str("");ss<<argv[2];ss>>tracks;
  
  
  char* outfile = argv[3];
  ofstream out;
  out.open(outfile);
  
  double mass = 0.1396;
  double px = 0.;
  double py = 0.;
  double pz = 0.;
  TF1 f1("momentum", "x/(0.05+x*x*x)", 3, 30.);
  TRandom3 rand;
  rand.SetSeed();
  
  for(unsigned int ev=0;ev<nevents;++ev)
  {
    for(unsigned int trk=0;trk<tracks;++trk)
    {
      double p = f1.GetRandom();
      double dzdl = rand.Uniform(-0.5, 0.5);
      double pz = p*dzdl;
      double pt = sqrt(p*p - pz*pz);
      double phi = rand.Uniform(0., 2.*TMath::Pi());
      double px = pt*cos(phi);
      double py = pt*sin(phi);
      double cg = rand.Uniform(-1., 1.);
      int charge = 1;
      if(cg < 0){charge = -1;}
      double vx = 0.;
      double vy = 0.;
      double vz = 0.;
      out<<ev<<" "<<charge<<" "<<mass<<" "<<px<<" "<<py<<" "<<pz<<" "<<vx<<" "<<vy<<" "<<vz<<"\n";
    }
  }
  
}
