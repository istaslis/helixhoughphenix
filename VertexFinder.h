#ifndef __VERTEXFINDER_H__
#define __VERTEXFINDER_H__

// Helix Hough includes
#include <SimpleTrack3D.h>

// standard includes
#include <vector>

/// \class VertexFinder
///
/// \brief A class to determine the vertex
///
/// This class incorporates Newton's method for gradient descent
/// against an expo-dca^2 function.
///
class VertexFinder
{

 public:
  
  VertexFinder();
  virtual ~VertexFinder() {}

  bool findVertex(std::vector<SimpleTrack3D>& tracks,std::vector<float>& vertex, float sigma); 
  
 protected:

 
};

#endif // __VERTEXFINDER_H__
