#!/bin/bash

g++ -O3 -pipe -DNDEBUG `pkg-config --cflags eigen3`  `pkg-config --cflags helix_hough` `pkg-config --cflags SimpleTrack`  `root-config --cflags` -c test_sPHENIX.cpp

g++ -o test_sPHENIX test_sPHENIX.o `pkg-config --libs helix_hough` `root-config --libs` `pkg-config --libs SimpleTrack`

