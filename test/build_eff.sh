#!/bin/bash

g++ -O3 -pipe -DNDEBUG `pkg-config --cflags SimpleTrack` `pkg-config --cflags tracking_evaluator`  `root-config --cflags` -c calculate_eff_sPHENIX.cpp 

g++ -o calculate_eff_sPHENIX calculate_eff_sPHENIX.o `root-config --libs` `pkg-config --libs SimpleTrack` `pkg-config --libs tracking_evaluator`

