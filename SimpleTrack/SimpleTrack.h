#ifndef __SIMPLETRACK__
#define __SIMPLETRACK__

#include "TObject.h"
#include <vector>


class SimpleMCHit : public TObject
{
public:
  SimpleMCHit(float xx=0., float yy=0., float zz=0., unsigned int ll=0, unsigned int ii=0) : x(xx), y(yy), z(zz), layer(ll), index(ii) {}
  ~SimpleMCHit(){}
  
  float x;
  float y;
  float z;
  unsigned int layer;
  unsigned int index;
  
  ClassDef(SimpleMCHit, 1);
};


class SimpleMCTrack : public TObject
{
  public:
    SimpleMCTrack() : kappa(0.), dzdl(0.), d(0.), phi(0.), z0(0.)  {}
    ~SimpleMCTrack(){}
    
    std::vector<SimpleMCHit> hits;
    float kappa;
    float dzdl;
    float d;
    float phi;
    float z0;
    
    ClassDef(SimpleMCTrack, 1);
};


class SimpleMCEvent : public TObject
{
  public:
    SimpleMCEvent(){}
    ~SimpleMCEvent(){}
    
    std::vector<SimpleMCTrack> tracks;
    
    ClassDef(SimpleMCEvent, 1);
};


class SimpleRecoTrack : public TObject
{
  public:
    SimpleRecoTrack() : kappa(0.), dzdl(0.), d(0.), phi(0.), z0(0.), chi2(0.), isolation(0.) {}
    ~SimpleRecoTrack(){}
    
    std::vector<unsigned int> indexes;
    float kappa;
    float dzdl;
    float d;
    float phi;
    float z0;
    
    float chi2;
    float isolation;
    
    ClassDef(SimpleRecoTrack, 1);
};


class SimpleRecoEvent : public TObject
{
  public:
    SimpleRecoEvent(){}
    ~SimpleRecoEvent(){}
    
    std::vector<SimpleRecoTrack> tracks;
    
    ClassDef(SimpleRecoEvent, 1);
};




#endif
