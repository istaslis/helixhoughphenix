
#include "VertexFinder.h"

// Helix Hough includes
#include <SimpleTrack3D.h>
#include <VertexFitFunc.h>

// FitNewton includes
#include <NewtonMinimizerGradHessian.h>

// Eigen includes
#include <Eigen/LU>
#include <Eigen/Core>

// standard includes
#include <iostream>

using namespace std;

using namespace FitNewton;
using namespace Eigen;

VertexFinder::VertexFinder()
{

}

/// The method will find the vertex given an initial guess and a list of
/// track candidates.
///
/// \param[in] tracks List of track candidates
/// \param[in,out] vertex The vertex position in x,y,z
///
/// \return true if successful
///
bool VertexFinder::findVertex(vector<SimpleTrack3D>& tracks, vector<float>& vertex, float sigma)
{
  VertexFitFunc _vertexfit;
  FitNewton::NewtonMinimizerGradHessian _minimizer;

  // setup function to minimize
  // expo-dca2 => ~dca^2 w/ extreme outlier de-weighting
  _vertexfit.setTracks(&tracks);
    
  // setup the minimizer
  // fast Newton gradient minimization
  _minimizer.setFunction(&_vertexfit);
    
  // set an initial guess and uncertainties
  VectorXd start_point = VectorXd::Zero(vertex.size()); // input initial guess
  for(unsigned int i=0;i<vertex.size();++i)
  {
    start_point(i) = vertex[i];
  }
  _vertexfit.setFixedPar(0, sigma);                     // index = 0 used for x,y,z uncertainties

  // output storage
  VectorXd min_point = VectorXd::Zero(vertex.size());   // output vector for minimize method below

  // minimize
  _minimizer.minimize(start_point, min_point, 1.0e-9, 1024, 1.0e-15);

  // store output vertex spatial point
  for(unsigned i=0; i<vertex.size(); i++)
    {
      vertex[i] = min_point(i);
    }

  return true;
}
